(function() {
  'use strict';

  const jwt = require('jsonwebtoken');
  const crypto = require('crypto');
  const httpResponse = require('http-response');
  const assign = require('object-assign');

  const defaults = {
    algorithm: process.env.COOKIE_ALGORITHM || 'sha256',
    expiresIn: process.env.COOKIE_EXPIRES_IN || '2 days',
    cookieName: process.env.COOKIE_NAME || 'depre-it',
    key: process.env.COOKIE_KEY || 'UFTKLV#@QcEvIek09xdC6*&0',
    salt: process.env.COOKIE_SALT || 'udhyttecveyycvvet%^53gh',

  };

  process.env.COOKIE_MAX_AGE && (defaults.maxAge = process.env.COOKIE_MAX_AGE);
  process.env.COOKIE_DOMAIN && (defaults.domain = process.env.COOKIE_DOMAIN);
  process.env.COOKIE_HTTP_RESPONSE_ONLY && (defaults.httpOnly = process.env.COOKIE_HTTP_RESPONE_ONLY);
  process.env.COOKIE_PATH && (defaults.path = defaults.env.COOKIE_PATH);
  process.env.COOKIE_SECURE && (defaults.secure = process.env.COOKIE_SECURE);
  process.env.COOKIE_SIGNED && (defaults.signed = process.env.COOKIE_SIGNED);
  process.env.COOKIE_SAMESITE && (defaults.sameSite = process.env.COOKIE_SAMESITE);

  function wrapper(options) {
    // if options are static (either via defaults or custom options passed in), wrap in a function
    if (!options) throw new Error('Some options are required, provide an options object.');
    let optionsCallback = null;
    if (typeof options === 'function') {
      optionsCallback = options;
    }
    else {
      if (!options.salt) throw new Error('salt is a quired option');
      optionsCallback = function(req, cb) {
        cb(null, options);
      };
    }

    return {
      read: function(req, res) {

        return new Promise(function(resolve, reject) {
          optionsCallback(req, function(err, options) {

            if (err) {
              reject(err);
            }
            else {
              const o = assign({}, defaults, options);
              try {
                const secret = crypto.createHash(o.algorithm).update(o.salt + o.key).digest('hex');
                var cookie = req.cookies[o.cookieName];
                if (cookie) {
                  jwt.verify(cookie, secret, function(err, decoded) {
                    if (err) {
                      reject(err);
                    }
                    else {
                      resolve(decoded.content);
                    }
                  });
                }
                else {
                  reject('Cookie not found');
                }
              }
              catch (e) {
                reject(e);
              }
            }
          });
        });
      },


      write: function(req, res, content) {
        return new Promise(function(resolve, reject) {
          return optionsCallback(req, function(err, options) {
            if (err) {
              reject(err);
            }
            else {
              const o = assign({}, defaults, options);
              try {
                const secret = crypto.createHash(o.algorithm).update(o.salt + o.key).digest('hex');
                jwt.sign({ content: content }, secret, {
                  expiresIn: o.expiresIn
                }, (err, content) => {
                  err && httpResponse.internalServerError(res)(err);
                  res.cookie(o.cookieName, content, options);
                  resolve(content);
                });
              }
              catch (e) {
                reject(e);
              }
            }
          });
        });
      },

      clear: function(req, res, content) {
        return new Promise(function(resolve, reject) {
          return optionsCallback(req, function(err, options) {
            if (err) {
              reject(err);
            }
            else {
              const o = assign({}, defaults, options);
              try {
                res.clearCookie(o.cookieName);
                resolve();
              }
              catch (e) {
                reject(e);
              }
            }
          });
        });
      }
    };
  }
  module.exports = wrapper;
}());
